# SmartMirror

Una Web App para utilizar como fuente de datos para un Smart Mirror.
La construcción de este "espejo inteligente" tiene como objetivo ser de bajo costo, y está hecho de piezas fácilmente disponibles.

Todo se basa en el uso de un marco de fotos normal, y un film espejado para convertir el cristal en un espejo de dos vías. A continuación, se encierra un monitor en un marco de madera y se fija el monitor al espejo. Por último, se ejecuta el software (o web, en este caso) y se coloca detrás del monitor.

Este proyecto también incluye un asistente de voz. Tu espejo inteligente será genial sin esto, pero es bueno tener un asistente de voz en tu espejo para hacer preguntas difíciles o controlar la automatización del hogar.

<img src="docs/Home.png">
<img src="docs/Sidebar.png">