﻿const URLAPIFRASE = "https://cors-anywhere.herokuapp.com/https://frasedeldia.azurewebsites.net/api/phrase";
// https://fraze.it/api.jsp

export function getFraseDelDia() {
  let apiClima = new XMLHttpRequest();
  apiClima.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let frase = JSON.parse(this.responseText)["phrase"];
      let autor = JSON.parse(this.responseText)["author"];
      actualizarSeccionFrase(frase, autor);
    }
  }
  apiClima.open("GET", URLAPIFRASE, true);
  apiClima.send();
}

function actualizarSeccionFrase(frase, autor) {
  document.getElementById("FraseTitulo").innerHTML = frase;
  document.getElementById("FraseAutor").innerHTML = autor+" ";
}