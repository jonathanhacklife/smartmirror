﻿const URLAPINOTICIAS = "https://cors-anywhere.herokuapp.com/https://newsapi.org/v2/top-headlines?country=ar&apiKey=2191acc559824063ae623e187f743b4d";
const TRANSICION_NOTICIAS = 10000;

export function getNoticias() {
  let apiNoticias = new XMLHttpRequest();
  apiNoticias.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var indice = 0;
      let respuesta = JSON.parse(this.responseText);
      let articulos = respuesta["articles"];
      let totalArticulos = respuesta["articles"].length - 1;

      /* INTERVALO DE ACTUALIZACIÓN */
      getDatosRelevantes();
      setInterval(function () {
        if (indice < totalArticulos) {
          indice++;
        } else {
          indice = 0;
        }
        getDatosRelevantes();
      }, TRANSICION_NOTICIAS);

      /* LISTENERS PARA NAVEGAR ENTRE LAS NOTICIAS */
      document.getElementById("siguienteNoticia").addEventListener("click", function () {
        if (indice < totalArticulos) {
          indice++;
          getDatosRelevantes();
        }
      }, false);
      document.getElementById("anteriorNoticia").addEventListener("click", function () {
        if (indice > 0) {
          indice--;
          getDatosRelevantes();
        }
      }, false);

      /* OPTIMIZACIÓN DE DATOS */
      function getDatosRelevantes() {
        let titulo = articulos[indice]["title"].substr(0, articulos[indice]["title"].lastIndexOf(" - "));
        let fuente = articulos[indice]["source"]["name"];
        let descripcion = articulos[indice]["description"];
        let url = articulos[indice]["url"];
        let imagen = articulos[indice]["urlToImage"];

        /* MOSTRAR EN PANTALLA */
        document.getElementById("imagenNoticia").src = imagen != undefined ? imagen : "";
        document.getElementById("TituloNoticia").innerHTML = titulo;
        document.getElementById("FuenteNoticias").innerHTML = `<a href="${url}" target="_blank">${fuente} [${indice} | ${totalArticulos}]`;
        document.getElementById("DescripcionNoticia").innerHTML = descripcion != undefined ? descripcion : "";
      }
    }
  }
  apiNoticias.open("GET", URLAPINOTICIAS, true);
  apiNoticias.send();
}