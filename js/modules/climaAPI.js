﻿const URL_CLIMA = "https://api.openweathermap.org/data/2.5/weather?q=Buenos%20Aires&units=metric&lang=sp&appid=a59f1666d2876de912dfa65ef267194b";

export function getClima() {
  let apiClima = new XMLHttpRequest();
  apiClima.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      let respuesta = JSON.parse(this.responseText);
      let temperatura = parseInt(respuesta["main"]["temp"]);
      let icono = "http://openweathermap.org/img/wn/" + respuesta["weather"][0]["icon"] + "@2x.png";
      let tipoClima = respuesta["weather"][0]["main"];
      let descripcion = respuesta["weather"][0]["description"];
      let max = parseInt(respuesta["main"]["temp_max"]);
      let min = parseInt(respuesta["main"]["temp_min"]);

      /* MOSTRAR EN PANTALLA */
      document.getElementById("Temperatura").innerHTML = temperatura + "°";
      document.getElementById("Descripcion").innerHTML = "🔻" + min + "° - 🔺" + max + "°<br>" + descripcion;
      document.getElementById("iconoClima").src = icono;
      switch (tipoClima) {
        case "Rain":
          document.getElementById("Principal").style.backgroundImage = "url('./img/lluvia.gif')";
          break;
        case "Thunderstorm":
          document.getElementById("Principal").style.backgroundImage = "url('./img/rempalagos.gif')";
          break;
        default:
          break;
      }

    }
  }
  apiClima.open("GET", URL_CLIMA, true);
  apiClima.send();
}