﻿import { getClima } from "./modules/climaAPI.js";
import { getNoticias } from "./modules/noticiasAPI.js";
import { getFraseDelDia } from "./modules/frasedeldia.js";

const OPCIONES = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
const ACTUALIZACION_DATOS = 300000;

/* FECHA Y HORA */
setInterval(function () {
  let fecha = new Date().toLocaleDateString(undefined, OPCIONES);
  let hora = new Date().toLocaleTimeString();
  document.getElementById("Fecha").innerHTML = fecha;
  document.getElementById("Hora").innerHTML = hora;
}, 1000);

/* COMPROBAR CONEXIÓN */
const comprobarConexion = () => window.navigator.onLine ? document.getElementById("Conexion").innerHTML = "🌐" : alert('Conexión no disponible')
document.getElementById("Conexion").addEventListener("click", function () { comprobarConexion() }, false);

/* VISUAL */
getClima();
setInterval(getClima, ACTUALIZACION_DATOS);
getNoticias();
getFraseDelDia();
comprobarConexion();

/* EVENT LISTENERS */
/* NAVBAR */
document.getElementById("Opciones").addEventListener("click", function () {
  document.getElementsByTagName("NAV")[0].style.width = "30vw";
  document.body.style.paddingLeft = "30vw";
})
document.getElementById("closebtn").addEventListener("click", function () {
  document.getElementsByTagName("NAV")[0].style.width = "0";
  document.body.style.paddingLeft = "0";
})

document.getElementById("btnAcerca").addEventListener("click", () => alert("Un espejo virtual o espejo inteligente (SmartMirror) es un dispositivo que muestra la propia imagen de un usuario en una pantalla como si esa pantalla fuera un espejo. Algunas versiones presentan adiciones de realidad aumentada a la pantalla de video, o usan un avatar gráfico completamente virtual del usuario\n\nEste proyecto fue elaborado con ❤ por Jonathan Hacklife"), false);

/* ACTIVAR TAREAS */
document.getElementById("btnTareas").addEventListener("click", () => document.getElementById("Tareas").style.display == "none" ? document.getElementById("Tareas").style.display = "block" : document.getElementById("Tareas").style.display = "none", false);

/* ACTIVAR FRASE DEL DIA */
document.getElementById("btnFraseDelDia").addEventListener("click", () => document.getElementById("FraseDelDia").style.display == "none" ?
  document.getElementById("FraseDelDia").style.display = "block" : document.getElementById("FraseDelDia").style.display = "none", false);

/* ACTIVAR NOTICIAS */
document.getElementById("btnNoticias").addEventListener("click", () => document.getElementById("SeccionNoticias").style.display == "none" ? document.getElementById("SeccionNoticias").style.display = "flex" : document.getElementById("SeccionNoticias").style.display = "none", false);

/* ACTIVAR FULLSCREEN */
document.getElementById("btnFullscreen").addEventListener("click", () => document.fullscreenElement ? document.exitFullscreen() : document.documentElement.requestFullscreen(), false);