﻿localStorage = window.localStorage;
class Tarea {
  constructor(titulo) {
    this.titulo = titulo;
  }

  get checked() {
    return this.check;
  }

  set checked(status) {
    this.check = status
  }
}

class ListaTareas {
  tareas = JSON.parse(localStorage.getItem('misTareas')) ?? [];

  crearTarea(titulo) {
    if (titulo.length > 0) {
      let nuevatarea = new Tarea(titulo);
      this.tareas.push(nuevatarea);
      this.mostrarTareas();
    }
    else {
      alert("Debe escribir algo.");
    }
  }

  eliminarTarea(indice) {
    this.tareas.splice(indice, 1);
    this.mostrarTareas();
  }

  completarTarea(indice) {
    if (this.tareas[indice].checked) {
      this.tareas[indice].checked = false;
    } else {
      this.tareas[indice].checked = true;
    }
    this.mostrarTareas();
  }

  mostrarTareas() {
    document.getElementById("listaTareas").innerHTML = ``;
    localStorage.setItem('misTareas', JSON.stringify(listadetareas.tareas));
    for (let i = 0; i < this.tareas.length; i++) {
      if (this.tareas[i].titulo.length > 30) {
        document.getElementById("listaTareas").innerHTML += `
      <li>${this.tareas[i].titulo.slice(0, 30)}...<button class="eliminarTarea">\u00D7</button></li>
      `;
      } else {
        document.getElementById("listaTareas").innerHTML += `
      <li>${this.tareas[i].titulo}<button id="${i}" class="eliminarTarea">\u00D7</button></li>
        `;
      }
      if (this.tareas[i].checked)
        document.getElementById("listaTareas").getElementsByTagName("LI")[i].classList.toggle('checked');

      let elementos = document.getElementById("listaTareas").getElementsByClassName("eliminarTarea");
      for (let i = 0; i < elementos.length; i++) {
        elementos[i].addEventListener("click", (e) => this.eliminarTarea(e.target.id));

        /* COMPLETAR TAREA */
        document.getElementById("listaTareas").getElementsByTagName("LI")[i].addEventListener("click", () => this.completarTarea(i), true);
      }
    }
  }
}

let listadetareas = new ListaTareas();
listadetareas.mostrarTareas();
document.getElementById("agregarTarea").addEventListener("click", () => {
  listadetareas.crearTarea(document.getElementById("entrada").value);
  document.getElementById("entrada").value = "";
});